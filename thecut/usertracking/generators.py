# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import uuid


def default_user_kwargs():
    return {
        'username': '{uuid}'.format(uuid=uuid.uuid4()).replace('-', '')[:30],
        'is_active': False,
        'is_staff': False,
        'is_superuser': False,
    }
