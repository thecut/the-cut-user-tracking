# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings


GROUP_NAME = getattr(settings, 'USERTRACKING_GROUP_NAME', 'user-tracking')


USER_KWARGS_GENERATOR = getattr(
    settings, 'USERTRACKING_USER_KWARGS_GENERATOR',
    'thecut.usertracking.generators.default_user_kwargs')


TRACKING_COOKIE_MAX_AGE = getattr(
    settings, 'USERTRACKING_TRACKING_COOKIE_MAX_AGE',
    60*60*24*90)  # 90 days (in seconds)


TRACKING_COOKIE_NAME = 'tracked_user_username'


# Deprecated

USER_COOKIE_MAX_AGE = TRACKING_COOKIE_MAX_AGE
