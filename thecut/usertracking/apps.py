# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from actstream import registry as actstream_registry
from django import apps
from django.contrib.auth import get_user_model


class AppConfig(apps.AppConfig):

    name = 'thecut.usertracking'

    def ready(self):
        actstream_registry.register(get_user_model())
