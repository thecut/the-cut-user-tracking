# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import exceptions, settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.signing import BadSignature
from django.utils.module_loading import import_string
import warnings


def create_tracked_user(request, response):
    # Create a new user based on the output of USER_KWARGS_GENERATOR()
    user_kwargs = import_string(settings.USER_KWARGS_GENERATOR)()
    user = get_user_model()(**user_kwargs)
    user.set_unusable_password()
    user.save()
    user.groups.add(get_tracked_group())
    # Track future user actions against this user.
    set_tracking_cookie(user=user, response=response)
    return user


def get_tracked_user(request, force_track=False):

    if not force_track and request.META.get('HTTP_DNT', False):
        raise exceptions.DoNotTrack()

    if request.user.is_authenticated():
        user = request.user
    else:
        UserModel = get_user_model()
        try:
            username = request.get_signed_cookie(settings.TRACKING_COOKIE_NAME)
            query_kwargs = {UserModel.USERNAME_FIELD: username}
            user = UserModel.objects.get(**query_kwargs)
        except (BadSignature, KeyError, UserModel.DoesNotExist) as exception:
            raise UserModel.DoesNotExist(exception)

    # Always ensure this user is part of the tracking group
    user.groups.add(get_tracked_group())

    return user


def get_or_create_tracked_user(request, response, force_track=False):

    if not force_track and request.META.get('HTTP_DNT', False):
        raise exceptions.DoNotTrack()

    created = False

    try:
        user = get_tracked_user(request=request, force_track=force_track)
        # Reset cookie (e.g. ensure we update expiration)
        set_tracking_cookie(user=user, response=response)
    except get_user_model().DoesNotExist:
        user = create_tracked_user(request=request, response=response)
        created = True

    return user, created


def get_tracked_group():
    group, created = Group.objects.get_or_create(name=settings.GROUP_NAME)
    return group


def set_tracking_cookie(user, response):
    response.set_signed_cookie(settings.TRACKING_COOKIE_NAME,
                               getattr(user, get_user_model().USERNAME_FIELD),
                               max_age=settings.TRACKING_COOKIE_MAX_AGE)


# Depreacted methods, for naming consistency


def create_tracking_user(*args, **kwargs):
    warnings.warn(
        'create_tracking_user is deprecated, use create_tracked_user instead.',
        DeprecationWarning, stacklevel=2)
    return create_tracked_user(*args, **kwargs)


def get_tracking_group(*args, **kwargs):
    warnings.warn(
        'get_tracking_group is deprecated, use get_tracked_group instead.',
        DeprecationWarning, stacklevel=2)
    return get_tracked_group(*args, **kwargs)


def get_tracking_user(*args, **kwargs):
    warnings.warn(
        'get_tracking_user is deprecated, use get_or_create_tracked_user '
        'instead.', DeprecationWarning, stacklevel=2)
    return get_or_create_tracked_user(*args, **kwargs)
