# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.auth import get_user_model


try:
    from django.apps import registry
except ImportError:
    # Pre-Django 1.7 compatibility
    from actstream import registry
    registry.register(get_user_model())
