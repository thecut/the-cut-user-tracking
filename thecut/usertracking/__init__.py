# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .exceptions import DoNotTrack
from .utils import (get_tracked_user, get_or_create_tracked_user,
                    get_tracking_user)
from actstream import action
from django.utils import timezone


__all__ = ['DoNotTrack', 'get_tracked_user', 'get_or_create_tracked_user',
           'get_tracking_user', 'track_user']


default_app_config = 'thecut.usertracking.apps.AppConfig'


def track_user(request, response, verb, target, action_object=None,
               description=None, timestamp=None, public=False,
               force_track=False):
    """Finds a user based on the request, and creates an action record."""

    try:
        user, created = get_or_create_tracked_user(
            request=request, response=response, force_track=force_track)
    except DoNotTrack:
        raise

    timestamp = timestamp or timezone.now()
    action.send(user, verb=verb, target=target,
                action_object=action_object, public=public,
                description=description, timestamp=timestamp)

    return user
