from setuptools import setup, find_packages
from version import get_git_version


setup(
    name='thecut-usertracking',
    author='The Cut',
    author_email='development@thecut.net.au',
    url='https://projects.thecut.net.au/projects/thecut-usertracking',
    namespace_packages=['thecut'],
    version=get_git_version(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=['django-activity-stream==0.6.2']
)
